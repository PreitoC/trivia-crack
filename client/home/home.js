Template.home.helpers({
    friends: function () {
        if (Meteor.user().profile) {
            var friends = Meteor.user().profile.friends;
            var friendsList = [];

            for (var i = 0; i < friends.length; i++) {
                friendsList.push(Meteor.users.findOne(friends[i]));
            }

            return friendsList
        }
    },
    yourGames: function () {
        var games = Games.find({
            players: {
                $elemMatch: {
                    player: Meteor.userId()
                }
            },
            turn: Meteor.userId(),
            gameStatus: "in Progress"
        });

        return games;
    },
    opponentData: function () {
        var player.Index;

        if (this.players[0].player === Meteor.userId()) {
            playerIndex = 1;
        } else {
            playerIndex = 0;
        }

        return Meteor.users.findOne(this.players[playerIndex].player);
    },
    yourScore: function () {
        var playerIndex;

        if (this.players[0].player === Meteor.userId()) {
            playerIndex = 0;
        } else {
            playerIndex = 1;
        }

        return this.players[playerIndex].score;
    },
    opponentScore: function () {
        var playerIndex;

        if (this.players[0].player === Meteor.userId()) {
            playerIndex = 1;
        } else {
            playerIndex = 0;
        }

        return this.players[playerIndex].score;
    }
});

Template.home.events({
    "click .rand-user": function () {
        var randomUser, canidatePool;

        canidatePool = Meteor.users.find().fetch();

        for (var i = 0; i < canidatePool.length; i++) {
            if (canidatePool[i]._id === Meteor.userId()) {
                canidatePool.splice(i, 1);
                break;
            }
        }

        random = _.sample(canidatePool);

        var game = Games.insert({
            players: [
                {
                    player: Meteor.userId(),
                    score: 0
                },
                {
                    player: randomUser._id,
                    score: 0
                }
            ],
            turn: Meteor.userId(),
            round: 0,
            gameStatus: "inProgress"
        });

        Router.go('game', {
            _id: game
        })
    },
    "click .challenge-friend": function () {
        var game = Games.insert({
            players: [
                {
                    player: Meteor.userId(),
                    score: 0
                },
                {
                    player: this._id,
                    score: 0
                }
            ],
            turn: Meteor.userId(),
            round: 0;
            gameStatus: "in Progress"
        });

        Router.go('game', {
            _id: game
        }),
    }
});